<?php 

class BlockPage extends Page
{
    private static $singular_name = 'Block-Seite';
    private static $plural_name = 'Block-Seiten';
    private static $description = 'Seite mit Inhalts-Blocks';

    private static $icon_class = 'font-icon-p-alt-2';

    private static $db = [];

    private static $many_many = [];

    private static $many_many_extraFields = [];

    private static $belongs_many_many = [];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }
    
}
