<?php

use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextareaField;

class PrivacyPage extends Page
{
    private static $singular_name = 'Datenschutz-Seite';
    private static $plural_name = 'Datenschutz-Seiten';
    private static $description = 'Seite mit vordefinierten Daten-Feldern für eine Datenschutzseite';

    private static $icon_class = 'font-icon-p-shield';

    private static $db = [
        'ResponsibleName' => 'Varchar(128)',
        'ResponsibleAddress' => 'Text',
        'ResponsiblePhone' => 'Varchar(128)',
        'ResponsibleFax' => 'Varchar(128)',
        'ResponsibleEmail' => 'Varchar(128)',
        'ResponsibleWebsite' => 'Varchar(128)',
        'PrivacyManagerName' => 'Varchar(128)',
        'PrivacyManagerAddress' => 'Text',
        'PrivacyManagerPhone' => 'Varchar(128)',
        'PrivacyManagerFax' => 'Varchar(128)',
        'PrivacyManagerEmail' => 'Varchar(128)',
        'PrivacyManagerWebsite' => 'Varchar(128)',
        'LogStorageDurationDays' => 'Int',
        'EmailStorageDurationDays' => 'Int',
        'GoogleFonts' => 'Boolean',
        'GoogleMapsEmbed' => 'Boolean',
        'GoogleRecaptcha' => 'Boolean',
        'GoogleTagManager' => 'Boolean',
        'GoogleAnalytics' => 'Boolean',
        'GoogleRemarketing' => 'Boolean',
        'GoogleConversionTracking' => 'Boolean',
        'FacebookPixel' => 'Boolean',
        'FacebookFanpage' => 'Boolean',
        'InstagramLink' => 'Boolean',
        'YoutubeEmbed' => 'Boolean',
        'YoutubeChannel' => 'Boolean',
        'VimeoEmbed' => 'Boolean',
        'VimeoChannel' => 'Boolean',
        'YumpuuEmbed' => 'Boolean',
        'IssuuEmbed' => 'Boolean',
        'CDNJS' => 'Boolean'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Content',
            'ResponsibleName',
            'ResponsibleAddress',
            'ResponsiblePhone',
            'ResponsibleFax',
            'ResponsibleWebsite',
            'PrivacyManagerName',
            'PrivacyManagerAddress',
            'PrivacyManagerPhone',
            'PrivacyManagerFax',
            'PrivacyManagerWebsite',
            'LogStorageDurationDays',
            'EmailStorageDurationDays',
            'GoogleFonts',
            'GoogleMapsEmbed',
            'GoogleRecaptcha',
            'GoogleTagManager',
            'GoogleAnalytics',
            'GoogleRemarketing',
            'GoogleConversionTracking',
            'FacebookPixel',
            'FacebookFanpage',
            'InstagramLink',
            'YoutubeEmbed',
            'YoutubeChannel',
            'VimeoEmbed',
            'VimeoChannel',
            'YumpuuEmbed',
            'IssuuEmbed',
            'CDNJS'
        ]);

        $fields->addFieldsToTab('Root', [
            new TabSet('PrivacyTab', _t('Privacy.PRIVACY', 'Privacy'))
        ]);

        $fields->addFieldsToTab('Root.PrivacyTab.Main', [
            LiteralField::create('GeneralSettingsHeadline', '<h2>' . _t('Privacy.GENERALSETTINGS', 'General settings') . '</h2>'),
            NumericField::create('LogStorageDurationDays',  _t('Privacy.LOGSTORAGEDURATIONINDAYS', 'Log storage duration in days'))->setHTML5(true),
            NumericField::create('EmailStorageDurationDays', _t('Privacy.EMAILSTORAGEDURATIONINDAYS', 'Email storage duration in days'))->setHTML5(true),

            LiteralField::create('ResponsibleNameHeader', '<h2 class="mt-5">' . _t('Privacy.RESPONSIBLENAME', 'Responsible') . '</h2>'),
            TextField::create('ResponsibleName', _t('Privacy.RESPONSIBLENAME', 'Responsible')),
            TextareaField::create('ResponsibleAddress', _t('Privacy.ADDRESS', 'Address'))->setRows(3),
            TextField::create('ResponsiblePhone', _t('Privacy.PHONE', 'Phone')),
            TextField::create('ResponsibleFax', _t('Privacy.FAX', 'Fax')),
            TextField::create('ResponsibleEmail', _t('Privacy.EMAIL', 'Email')),

            LiteralField::create('AdditionalNotesHeader', '<h2 class="mt-5">' . _t('Privacy.ADDITIONALNOTES', 'Additional notes') . '</h2>'),
            HTMLEditorField::create('Content', _t('Privacy.CONTENT', 'Content')),

            LiteralField::create('PrivacyManagerHeader', '<h2 class="mt-5">' . _t('Privacy.PRIVACYMANAGERNAME', 'Privacy manager') . '</h2>'),
            TextField::create('PrivacyManagerName', _t('Privacy.PRIVACYMANAGERNAME', 'Privacy manager')),
            TextareaField::create('PrivacyManagerAddress', _t('Privacy.ADDRESS', 'Address'))->setRows(3),
            TextField::create('PrivacyManagerPhone', _t('Privacy.PHONE', 'Phone')),
            TextField::create('PrivacyManagerFax', _t('Privacy.FAX', 'Fax')),
            TextField::create('PrivacyManagerEmail', _t('Privacy.EMAIL', 'Email')),
            TextField::create('PrivacyManagerWebsite', _t('Privacy.WEBSITE', 'Website')),

            LiteralField::create('UsedServicesHeader', '<h2 class="mt-5">' . _t('Privacy.USEDSERVICES', 'Used services') . '</h2>'),
            CheckboxField::create('GoogleFonts', 'Google Fonts'),
            CheckboxField::create('GoogleMapsEmbed', 'GoogleMaps Embed'),
            CheckboxField::create('GoogleRecaptcha', 'Google Recaptcha'),
            CheckboxField::create('GoogleTagManager', 'Google Tag-Manager'),
            CheckboxField::create('GoogleAnalytics', 'Google-Analytics'),
            CheckboxField::create('GoogleRemarketing', 'Google-Remarketing'),
            CheckboxField::create('GoogleConversionTracking', 'Google Conversion-Tracking'),

            CheckboxField::create('FacebookPixel', 'Facebook-Pixel'),
            CheckboxField::create('FacebookFanpage', 'Facebook-Fanpage'),
            CheckboxField::create('InstagramLink', 'Instagram-Link'),

            CheckboxField::create('YoutubeEmbed', 'Youtube Embed'),
            CheckboxField::create('YoutubeChannel', 'Youtube-Channel'),

            CheckboxField::create('VimeoEmbed', 'Vimeo Embed'),
            CheckboxField::create('VimeoChannel', 'Vimeo-Channel'),

            CheckboxField::create('YumpuuEmbed', 'Yumpuu Embed'),
            CheckboxField::create('IssuuEmbed', 'Issuu Embed'),

            CheckboxField::create('CDNJS', 'cdn-js.com')
        ]);

        return $fields;
    }
}
