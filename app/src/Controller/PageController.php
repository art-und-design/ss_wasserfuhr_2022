<?php

namespace {

use SilverStripe\Forms\HiddenField;
use SilverStripe\View\Requirements;

    use rdoepner\CleverReach\ApiManager;
    use rdoepner\CleverReach\Http\Guzzle;
    use SilverStripe\Forms\Form;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\EmailField;
    use SilverStripe\Forms\FormAction;
    use SilverStripe\Forms\LiteralField;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\RequiredFields;
    use SilverStripe\SiteConfig\SiteConfig;

    use SilverStripe\CMS\Controllers\ContentController;

    class PageController extends ContentController
    {
        private static $allowed_actions = [
            'NewsletterForm',
            'sendNewsletterForm'
        ];

        protected function init(){
            parent::init();
            $siteConfig = SiteConfig::current_site_config();
            if($siteConfig->RecaptchaPublic){
                Requirements::javascript('https://www.google.com/recaptcha/api.js?render='.$siteConfig->RecaptchaPublic.'');
            }
        }

        public function NewsletterForm()
        {
            $form = Form::create(
                $this,
                __FUNCTION__,
                FieldList::create(
                    EmailField::create('email', 'Kostenlos anmelden')->setAttribute('placeholder', 'E-Mail Adresse')->setTitle('')->setCustomValidationMessage('Dies ist ein Pflichtfeld')->setFieldHolderTemplate('Forms/FormField_holder')->setTemplate('Forms/TextField'),
                    HiddenField::create('google-token')->addExtraClass('google-token-field'),
                ),
                FieldList::create([
                    FormAction::create('sendNewsletterForm', 'Jetzt kostenlos abonnieren')->addExtraClass('one-clicker mx-auto btn btn-outline-primary'),
                ]),
                RequiredFields::create('email')
            );

            return $form;
        }

        public function sendNewsletterForm($data, $form)
        {
            $formStatus = '';
            $formMessage = '';

            $siteConfig = SiteConfig::current_site_config();

            /**
             * Google ReCaptcha Test
             */
            $request = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$siteConfig->RecaptchaSecret.'&response='.$data['google-token']);
            $request = json_decode($request);
            
            if($request->success == true){
                if($request->score < 0.6){
                    $formStatus = 'bad alert alert-danger';
                    $formMessage = _t('Recaptcha.SPAMBOT', 'It seems you have been flagged as a spam bot. But if you are human, just try again.');
                    $form->sessionMessage($formMessage, $formStatus);
                    return $this->redirectBack();
                }
            }else{
                $formStatus = 'bad alert alert-danger';
                $formMessage = _t('Recaptcha.SPAMERROR', 'Spam check failed. Please try again later.');
                $form->sessionMessage($formMessage, $formStatus);
                return $this->redirectBack();
            }

            /**
             * Generiere ein neues Access-Token und breche den Vorgang ab, 
             * wenn dies nicht funktioniert haben sollte
             */
            $httpAdapter = new Guzzle();
            $response = $httpAdapter->authorize($siteConfig->CleverreachClientID, $siteConfig->CleverreachClientSecret);

            if (!isset($response['access_token'])) {
                $formStatus = 'bad alert alert-danger';
                $formMessage = 'Die Newsletternameldung ist nicht richtig konfiguriert. Bitte wenden Sie sich an den Betreiber.';
                $form->setRedirectToFormOnValidationError(true);
                $form->sessionMessage($formMessage, $formStatus);
                return $this->redirectBack();  
            }  

            $connection = new Guzzle(['access_token' => $response['access_token']]);
            $apiManager = new ApiManager($connection);

            /**
             * Versuche einen User in der Liste zu finden der 
             * die gleiche Mail-Adresse hat.
             */
            $reactivateResponse = $apiManager->getSubscriber(
                $data['email'],
                $siteConfig->CleverreachListID
            );

            /**
             * Sollte es einen Treffer geben, ist der User bereits angemeldet. 
             * Er ist aktiv oder deaktivivert. Starte den OPT-IN Prozess erneut 
             * und gib dem User ein Feedback.
             */
            if (isset($reactivateResponse['id'])) {
                $reactivateResponse = $apiManager->triggerDoubleOptInEmail($data['email'], $siteConfig->CleverreachFormsID);
                $formStatus = 'good  alert alert-success alert alert-success';
                $formMessage = 'Diese E-Mail-Adresse befindet sich bereits in unserer Datenbank. Sollten Sie noch nicht aktiviert sein, senden wir Ihnen die Bestätigungsmail erneut.';
                $form->setRedirectToFormOnValidationError(true);
                $form->sessionMessage($formMessage, $formStatus);
                return $this->redirectBack();
            }

            /**
             * Sollte es bis heir keine Probleme geben, erstelle den 
             * User neu aber als inaktiv.
             */
            $createresponse = $apiManager->createSubscriber(
                $data['email'],
                $siteConfig->CleverreachListID,
                false
            );

            /**
             * Wenn alles klappt starte den OPT-IN Prozess.
             * Sollte etwas schief laufen gib dem User ein Feedback.
             */
            if (isset($createresponse['id'])) {
                $createresponse = $apiManager->triggerDoubleOptInEmail($data['email'], $siteConfig->CleverreachFormsID);
                if (isset($createresponse['success'])) {
                    $formStatus = 'good alert alert-success';
                    $formMessage = 'Anmeldung erfolgreich! Bitte prüfen Sie ihr Postfach.';
                } elseif (isset($createresponse['error'])) {
                    $formStatus = 'bad alert alert-danger';
                    $formMessage = 'Anmeldung erfolgreich! OPT-IN fehlerhaft. Bitte versuchen Sie es erneut.';
                }
            } elseif (isset($createresponse['error'])) {
                if ($createresponse['error']['code'] == 400) {
                    $formStatus = 'bad alert alert-danger';
                    $formMessage = 'Anmeldung nicht erfolgreich! Sie sind bereits in der Datenbank.';
                } else {
                    $formStatus = 'bad alert alert-danger';
                    $formMessage = 'Anmeldung nicht erfolgreich! Bitte versuchen Sie es später erneut.';
                }
            }

            $form->setRedirectToFormOnValidationError(true);
            $form->sessionMessage($formMessage, $formStatus);
            return $this->redirectBack();
        }

    }
}
