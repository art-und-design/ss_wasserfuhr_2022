<?php

use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ImprintPage extends Page
{
    private static $singular_name = 'Impressum';
    private static $plural_name = 'Impressen';
    private static $description = 'Seite mit vordefinierten Feldern und Funktionen für ein vollständiges Impressum';

    private static $icon_class = 'font-icon-p-book';

    private static $db = [
        'ResponsibleName' => 'Varchar(128)',
        'ResponsibleAddress' => 'Text',
        'ResponsiblePhone' => 'Varchar(128)',
        'ResponsibleFax' => 'Varchar(128)',
        'ResponsibleEmail' => 'Varchar(128)',
        'ResponsibleWebsite' => 'Text',

        'Webdesign' => 'Text',

        'TaxID' => 'Varchar(128)',
        'HRBNumber' => 'Varchar(128)',
        'DistrictCourt' => 'Varchar(128)',
        'ExecutiveDirectors' => 'Text'
    ];

    private static $has_one = [];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'ResponsibleName',
            'ResponsibleAddress',
            'ResponsiblePhone',
            'ResponsibleFax',
            'ResponsibleWebsite',
            'ResponsibleEmail',

            'Webdesign',
            'Content',

            'TaxID',
            'HRBNumber',
            'DistrictCourt',
            'ExecutiveDirectors'
        ]);

        $fields->addFieldsToTab('Root', array(
            new TabSet('ImprintTab', 'Impressum')
        ));

        $fields->addFieldsToTab('Root.ImprintTab.Main', [
            TextField::create('ResponsibleName', 'Verantwortlicher Betreiber'),
            TextareaField::create('ResponsibleAddress', 'Adresse')->setRows(3),
            TextField::create('ResponsiblePhone', 'Telefon'),
            TextField::create('ResponsibleFax', 'Fax'),
            TextField::create('ResponsibleEmail', 'Email'),
            TextField::create('ResponsibleWebsite', 'Website'),

            TextField::create('Webdesign', 'Webdesign'),
            HTMLEditorField::create('Content', 'Inhalt')->setTitle('Zusätzlicher Inhalt'),

            TextField::create('TaxID', 'USt.Nr.'),
            TextField::create('HRBNumber', 'HRB/HRA Nummer'),
            TextField::create('DistrictCourt', 'Amtsgericht'),
            TextareaField::create('ExecutiveDirectors', 'Geschäftsführer')
        ]);

        return $fields;
    }

    public function Attributions(){
        $images = Image::get()->filter(['InAttributions' => true]);
        return $images;
    }
}