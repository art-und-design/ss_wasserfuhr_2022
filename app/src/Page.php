<?php

namespace {

use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldGroup;
use SilverStripe\Forms\HeaderField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\TextareaField;
use Bummzack\SortableFile\Forms\SortableUploadField;
use SilverStripe\AssetAdmin\Forms\UploadField;

class Page extends SiteTree
    {
        private static $db = [
            'OGTitle' => 'Text',
            'OGType' => 'Varchar(16)',
            'OGDescription' => 'Text',

            'DisabledInMenu' => 'Boolean',
            'IsButtonInMenu' => 'Boolean'
        ];

        private static $many_many = [
            'OGImages' => Image::class
        ];

        private static $has_one = [
            'HeaderImage' => Image::class
        ];

        private static $owns = [
            'OGImages',
            'HeaderImage'
        ];

        private static $field_labels = [
            'OGType'        => 'Seitentyp',
            'OGImages'      => 'Bild/er',
            'OGTitle'       => 'Titel',
            'OGDescription' => 'Beschreibung',
            'OGURL'         => 'URL',
        ];

        private static $many_many_extraFields = [
            'OGImages' => ['SortOrder' => 'Int']
        ];

        public function getSettingsFields()
        {
            $fields = parent::getSettingsFields();

            $fields->addFieldsToTab("Root.Settings", [
                FieldGroup::create('', [
                    CheckboxField::create('DisabledInMenu', 'Im Menü deaktiviert'),
                    CheckboxField::create('IsButtonInMenu', 'Als Button darstellen')
                ])->setTitle('Menü-Darstellung')
            ], 'Sichtbarkeit');

            return $fields;
        }

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            $fields->removeByName([
                'OGTitle',
                'OGType',
                'OGDescription',
                'OGImages',
                'OGURL',
                'HeaderImage'
            ]);

            $fields->insertAfter('MenuTitle',
                UploadField::create('HeaderImage', 'Header-Bild'),
            );

            $fields->addFieldsToTab('Root.Main', [                
                HeaderField::create('OGTagsCMSHeadline', 'Open-Graph Tags')->addExtraClass('mt-5'),
                LiteralField::create(null, '<p>Bestimmen Sie selbst welche Bilder, Titel und Texte bei Facebook, Twitter und anderen Netzwerken auftauchen, wenn jemand diese Seite teilt oder postet. Setzen Sie hier keine eigenen Inhalte, generieren viele Netzwerke gegebenenfalls eigene Vorschauen. Es ist somit möglich, das Inhalte in den sozialen Netzwerken erscheinen, die gegenüber individuellen Inhalten gar nicht relevant oder sogar fehlerhaft sein können.</p>'),
                LiteralField::create(null, '<p class="small pb-3 border-bottom">Sie brauchen keine Bildformate oder Größen beachten. Laden Sie einfach ein ausreichend großes Bild hoch (mindestens 1200 Pixel breit) und das System kümmert sich um den Rest.</p>'),
                DropdownField::create('OGType', $this->fieldLabel('OGType'), ['website' => 'Eine einfache Internetseite', 'article' => 'Ein News-Artikel', 'blog' => 'Ein Beitrag aus Ihrem Blog'])->setDescription('Beschreiben Sie diese Seite hier so genau wie möglich.'),
                SortableUploadField::create('OGImages', $this->fieldLabel('OGImages'))->setSortColumn('SortOrder')->setDescription('Es können auch mehrere Bilder verwendet werden.'),
                TextField::create('OGTitle', $this->fieldLabel('OGTitle'))->setDescription('Freilassen des Feldes hat zur Folge, dass der Titel der Seite verwendet wird.'),
                TextareaField::create('OGDescription', $this->fieldLabel('OGDescription'))->setDescription('Freilassen des Feldes hat zur Folge, dass die Meta-Beschreibung der Seite verwendet wird.'),
                ReadonlyField::create('OGURL', $this->fieldLabel('OGURL'), $this->AbsoluteLink())->setDescription('Dieses Feld ist nicht veränderbar und dient nur der Information.'),
            ]);

            return $fields;
        }
    }
}
