<?php

use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Control\Director;
use SilverStripe\Forms\EmailField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\ListboxField;
use SilverStripe\Forms\LiteralField;
use rdoepner\CleverReach\Http\Guzzle;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\AssetAdmin\Forms\UploadField;

class CustomSiteConfig extends DataExtension
{
    private static $db = [
        'GEORegion' => 'Varchar(8)',
        'GEOPlacename' => 'Text',
        'GEOPositionLat' => 'Varchar(16)',
        'GEOPositionLong' => 'Varchar(16)',

        'FacebookDVC' => 'Text',

        'GoogleTagManagerCode'  => 'Varchar(128)',

        'FacebookLink' => 'Text',
        'YoutubeLink' => 'Text',
        'VimeoLink' => 'Text',
        'InstagramLink' => 'Text',
        'GoogleReviewsLink' => 'Text',
        'GoogleRouteLink' => 'Text',

        'FooterCompanyDataHeadline' => 'Text',
        'FooterCompanyDataAddress' => 'Text',
        'FooterCompanyDataPhone' => 'Text',
        'FooterCompanyDataFax' => 'Text',
        'FooterCompanyDataEmail' => 'Text',
        'FooterCompanyDataNotes' => 'Text',
        'FooterOpeningtimesHeadline' => 'Text',
        'FooterOpeningtimes' => 'Text',
        'FooterOpeningtimesNotes' => 'Text',
        'FooterQuicklinksHeadline' => 'Text',
        'FooterQuicklinksNotes' => 'Text',

        'NavbarPhone' => 'Text',
        'NavbarRoute' => 'Text',
        'NavbarEmail' => 'Text',

        'CleverreachClientID' => 'Text',
        'CleverreachClientSecret' => 'Text',
        'CleverreachListID' => 'Text',
        'CleverreachFormsID' => 'Text',

        'RecaptchaPublic' => 'Varchar(128)',
        'RecaptchaSecret' => 'Varchar(128)',
    ];

    private static $has_one = [
        'OGFallbackImage' => Image::class,

        'LogoBrand' => File::class,
        'LogoBrandMobile' => File::class,

        'ImportantImprintPage' => SiteTree::class,
        'ImportantContactPage' => SiteTree::class,
        'ImportantPrivacyPage' => SiteTree::class
    ];

    private static $has_many = [];

    private static $many_many = [
        'FooterQuicklinks' => SiteTree::class
    ];

    private static $many_many_extraFields = [];

    private static $owns = [
        'OGFallbackImage',

        'LogoBrand',
        'LogoBrandMobile'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab('Root', array(
            new TabSet('OGTags', 'Open-Graph'),
            new TabSet('GEODataTab', 'GEO-Daten'),
            new TabSet('GoogleTab', 'Google'),
            new TabSet('FacebookInstaTab', 'Facebook & Insta'),
            new TabSet('NewsletterTab', 'Newsletter'),
            new TabSet('MiscTab', 'Verschiedenes'),
            new Tabset('FooterTab', 'Footer')
        ));

        $fields->addFieldsToTab("Root.Main", array(
            UploadField::create('LogoBrandMobile', 'Logo Mobilgeräte')->setAllowedExtensions(['jpg', 'png', 'svg', 'jpeg']),
            UploadField::create('LogoBrand', 'Logo')->setAllowedExtensions(['jpg', 'png', 'svg', 'jpeg']),
            TextField::create('NavbarPhone', 'Telefonnummer für Navi-Leiste'),
            TextField::create('NavbarRoute', 'Route für Navi-Leiste'),
            EmailField::create('NavbarEmail', 'E-Mail für Navi-Leiste'),
            TreeDropdownField::create('ImportantImprintPageID', 'Impressum', SiteTree::class),
            TreeDropdownField::create('ImportantContactPageID', 'Kontakt', SiteTree::class),
            TreeDropdownField::create('ImportantPrivacyPageID', 'Datenschutz', SiteTree::class),
        ));

        $fields->addFieldsToTab('Root.OGTags.Main', [
            UploadField::create('OGFallbackImage', 'Open-Graph: Ersatz-Bild')
        ]);

        $fields->addFieldsToTab('Root.GEODataTab.Main', array(
            TextField::create('GEORegion', 'Region')->setDescription('Bitte im Format LAND-BUNDELAND angeben. Zum Beispiel: DE-BW für Deutschland, Baden-Würtemberg'),
            TextField::create('GEOPlacename', 'Name des Ortes')->setDescription('Bitte geben Sie den Namen des Ortes an. Zum Beispiel den Namen der Stadt oder der Firma'),
            TextField::create('GEOPositionLat', 'Breitengrad')->setDescription('Bitte im Format XX.XXXXX angeben'),
            TextField::create('GEOPositionLong', 'Längengrad')->setDescription('Bitte im Format XX.XXXXX angeben')
        ));

        $fields->addFieldsToTab('Root.FacebookInstaTab.Main', [
            TextField::create('FacebookDVC', 'Facebook Domain-Verification Key')->setDescription('Hier tragen Sie den Key zur Domain-Verification ein.'),
            TextField::create('FacebookLink', 'Facebook')->setDescription('Setzen Sie hier den Link zu Ihrer Facebook-Seite ein.'),
            TextField::create('InstagramLink', 'Instagram')->setDescription('Setzen Sie hier den Link zu Ihrer Instagram-Seite ein.'),
        ]);

        $fields->addFieldsToTab("Root.GoogleTab.Main", [
            TextField::create('GoogleTagManagerCode', 'Google Tag-Manager Code')->setDescription('Hier können Sie den Google Tag-Manager Code einsetzen'),
            TextField::create('GoogleReviewsLink', 'Google-Rezensionen')->setDescription('Setzen Sie hier den Link zu Ihrem Rezensionsformular ein.'),
            TextField::create('GoogleRouteLink', 'Google-Route')->setDescription('Setzen Sie hier den Link zu Ihrer Google-Maps Route ein.'),
            TextField::create('YoutubeLink', 'Youtube-Kanal')->setDescription('Setzen Sie hier den Link zu Ihrem Youtube-Kanal ein.'),
            TextField::create('RecaptchaPublic', 'Öffentlicher ReCaptcha-Code'),
            TextField::create('RecaptchaSecret', 'Geheimer ReCaptcha-Code'),
        ]);

        $fields->addFieldsToTab('Root.OGTags.Main', [
            UploadField::create('OGFallbackImage', 'Open-Graph: Ersatz-Bild')
        ]);

        $fields->addFieldsToTab("Root.NewsletterTab.Main", array(
            TextField::create('CleverreachClientID', 'CleverReach Kunden-ID')->setDescription('In CleverReach zunächst OAuth App erstellen und dann "&lt;CLIENT_ID&gt;" hier eintragen.'),
            TextField::create('CleverreachClientSecret', 'CleverReach Kunden-Kennwort')->setDescription('In CleverReach zunächst OAuth App erstellen und dann "&lt;CLIENT_SECRET&gt;" hier eintragen.'),
            TextField::create('CleverreachListID', 'CleverReach Empfängerliste')->setDescription('In CleverReach Empfängerliste auswählen, unter Einstellungen > Allgemeines die Listen ID hier eintragen.'),
            TextField::create('CleverreachFormsID', 'CleverReach Formulare')->setDescription('In CleverReach Formulare der Empfängerliste auswählen, unter Einstellungen > Informationen die Formular ID hier eintragen.'),
        ));

        $fields->addFieldsToTab("Root.FooterTab.Main", array(
            LiteralField::create(null, '<h2>Spalte Firmendaten</h2>'),
            TextField::create('FooterCompanyDataHeadline', 'Überschrift'),
            TextareaField::create('FooterCompanyDataAddress', 'Adresse')->setRows(4),
            TextField::create('FooterCompanyDataPhone', 'Telefonnummer'),
            TextField::create('FooterCompanyDataFax', 'Faxnummer'),
            EmailField::create('FooterCompanyDataEmail', 'E-Mail'),
            TextareaField::create('FooterCompanyDataNotes', 'Zusätzliche Hinweise'),

            LiteralField::create(null, '<h2 class="mt-5">Spalte Öffnungszeiten</h2>'),
            TextField::create('FooterOpeningtimesHeadline', 'Überschrift'),
            TextareaField::create('FooterOpeningtimes', 'Öffnungszeiten')->setRows(7),
            TextareaField::create('FooterOpeningtimesNotes', 'Zusätzliche Hinweise'),

            LiteralField::create(null, '<h2 class="mt-5">Spalte Quicklinks</h2>'),
            TextField::create('FooterQuicklinksHeadline', 'Überschrift'),
            ListboxField::create('FooterQuicklinks', 'Quicklinks', SiteTree::get()),
            TextField::create('FooterQuicklinksNotes', 'Zusätzliche Hinweise')
        ));
    }

    public function Multiply($in = 0, $mult = 1)
    {
        return $in * $mult;
    }

    public function Percent2Decimal($in = 0)
    {
        return $in / 100;
    }

    public function EnvironmentType()
    {
        if (Director::isLive()) {
            return 'live';
        } elseif (Director::isTest()) {
            return 'test';
        } elseif (Director::isDev()) {
            return 'dev';
        }
    }

    public function CallablePhonenumber($no = '', $tel = true)
    {
        $return = $tel || $tel == 'true' ? 'tel:' : '';
        return $return . (preg_replace('/[^0-9]/', '', $no));
    }

    public function getGTMCodeAttr()
    {
        return $this->owner->GoogleTagManagerCode ? "data-gtmcode=" . $this->owner->GoogleTagManagerCode . "" : '';
    }

    public function getIsHTTPS()
    {
        return Director::is_https();
    }

    public function getServices()
    {
        return Service::get()->Sort('SortOrder ASC');
    }

    public function getReviews()
    {
        return Review::get()->Sort('RatingDate DESC');
    }

    /**
     * Erzeug und verknüpft einen PageController mit einer Page
     * die dann dafür genutzt werden damit überall im Footer dieses
     * Formular verwendet werden kann.
     * 
     * @return Form
     */
    public function ShowNewsletterForm()
    {
        $aPage = Page::get()->first();
        return PageController::create($aPage)->NewsletterForm();
    }

    public function getGRCPublicAttr()
    {
        return $this->owner->RecaptchaPublic ? "data-grcpublic=" . $this->owner->RecaptchaPublic . "" : '';
    }

    public function MailSplittedInAttributes($email = ''){
        return LiteralField::create(null, 'data-receiver="'.explode('@', $email)[0].'" data-domain="'.explode('@', $email)[1].'"');
    }
}
