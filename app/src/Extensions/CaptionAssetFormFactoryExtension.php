<?php

use SilverStripe\Core\Extension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\CheckboxField;

class CaptionAssetFormFactoryExtension extends Extension
{
    public function updateFormFields(FieldList $fields)
    {
        $fields->insertAfter(
            'Title',
            TextField::create('Caption', 'Bildtext')
        );

        $fields->insertAfter(
            'Caption',
            TextField::create('SourceAuthor', 'Autor')
        );

        $fields->insertAfter(
            'SourceAuthor',
            TextField::create('SourceLink', 'Link zur Quelle')
        );

        $fields->insertAfter(
            'SourceLink',
            CheckboxField::create('InAttributions', 'Zeige in Bildnachweisen')
        );
    }
}