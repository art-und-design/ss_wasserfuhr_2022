<?php

use SilverStripe\Forms\Tab;
use SilverStripe\Forms\Tip;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HeaderField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\AssetAdmin\Forms\UploadField;

class BaseElementExtension extends DataExtension
{
    private static $db = [
        'MarginTop' => 'Int',
        'MarginBottom' => 'Int',
        'PaddingTop' => 'Int',
        'PaddingBottom' => 'Int',
        'BackgroundColor' => 'Varchar(16)',
        'BGImageOpacity' => 'Int',
        'BGAttachment' => 'Varchar(6)',
        'BGRepeat' => 'Varchar(9)',
        'BGPosition' => 'Varchar(16)',
        'BGPositionOffsetX' => 'Varchar(6)',
        'BGPositionOffsetY' => 'Varchar(6)',
        'BGSize' => 'Varchar(16)',
        'BGSizeCustom' => 'Varchar(64)',
        'BGMixFilter' => 'Varchar(32)'
    ];

    private static $defaults = [
        'MarginTop' => 0,
        'MarginBottom' => 0,
        'PaddingTop' => 0,
        'PaddingBottom' => 0,
        'BackgroundColor' => 'white',
        'BGImageOpacity' => 100,
        'BGAttachment' => 'scroll',
        'BGRepeat' => 'no-repeat',
        'BGPosition' => 'center center',
        'BGPositionOffsetX' => '0px',
        'BGPositionOffsetY' => '0px',
        'BGSize' => 'cover',
        'BGSizeCustom' => '100px auto',
        'BGMixFilter' => 'normal'
    ];

    private static $has_one = [
        'BGImage' => Image::class
    ];

    private static $owns = [
        'BGImage'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $distances = [0 => 'Kein Abstand', 1 => 'Sehr klein', 2 => 'Klein', 3 => 'Mittel', 4 => 'Groß', 5 => 'Sehr groß', 6 => 'Extrem'];

        $fields->addFieldsToTab('Root.Settings', [
            TabSet::create('InnerSettingsTabs', [
                Tab::create('BackgroundTab', 'Hintergrund'),
                Tab::create('DistancesTab', 'Abstände'),
                Tab::create('MiscTab', 'Sonstiges'),
            ])
        ]);

        $fields->addFieldsToTab('Root.Settings.InnerSettingsTabs.MiscTab', [
            $fields->fieldByName('Root.Settings.ExtraClass')
        ]);

        $fields->addFieldsToTab('Root.Settings.InnerSettingsTabs.DistancesTab', [
            DropdownField::create('PaddingTop', 'Innen-Abstand oben', $distances),
            DropdownField::create('PaddingBottom', 'Innen-Abstand unten', $distances),
            DropdownField::create('MarginTop', 'Aussen-Abstand oben', $distances),
            DropdownField::create('MarginBottom', 'Aussen-Abstand unten', $distances),
        ]);

        $fields->addFieldsToTab('Root.Settings.InnerSettingsTabs.BackgroundTab', [
            DropdownField::create('BackgroundColor', 'Hintergrundfarbe', [
                'primary' => 'Primärfarbe',
                'secondary' => 'Sekundärfarbe',
                'light' => 'Hell',
                'dark' => 'Dunkel',
                'white' => 'Weiß',
            ]),
            UploadField::create('BGImage', 'Bild')->setFolderName('Uploads/Hintergrundbilder'),
            NumericField::create('BGImageOpacity', 'Deckkraft in %')->setHTML5(true)->setScale(0)->setTip(new Tip('Testtip'))->setMaxLength(3),
            DropdownField::create('BGAttachment', 'Fixierung', [
                'scroll' => 'Scrollt mit',
                'fixed' => 'Fixiert'
            ]),
            DropdownField::create('BGMixFilter', 'Mix-Filter', [
                'normal' => 'Normal',
                'color-dodge' => 'Farbig abwedeln',
                'color-burn' => 'Farbig nachbelichten',
                'darken' => 'Abdunkeln',
                'difference' => 'Differenz',
                'hard-light' => 'Hartes Licht',
                'luminosity' => 'Luminanz',
                'multiply' => 'Mulitplizieren',
                'overlay' => 'Überlagern',
                'soft-light' => 'Weiches Licht'
            ]),
            DropdownField::create('BGRepeat', 'Wiederholung', [
                'no-repeat' => 'Keine Wiederholung',
                'repeat' => 'In alle Richtungen',
                'repeat-x' => 'Horizontal',
                'repeat-y' => 'Vertikal'
            ]),

            DropdownField::create('BGSize', 'Größe', [
                'cover' => 'Ausfüllend',
                'contain' => 'Passend',
                'custom' => 'Benutzerdefiniert'
            ]),

            TextField::create('BGSizeCustom', 'Benutzerdefinierte Größe')->setDescription('Angaben können in px, rem, em, % oder auto sein. Bsp.: "200px auto" oder "auto 100%"')->displayIf('BGSize')->isEqualTo('custom')->end(),

            DropdownField::create('BGPosition', 'Position', [
                'center center' => 'Mitte mitte',
                'center top' => 'Mitte oben',
                'right top' => 'Rechts oben',
                'right center' => 'Rechts Mitte',
                'right bottom' => 'Rechts unten',
                'center bottom' => 'Mitte unten',
                'left bottom' => 'Links unten',
                'left center' => 'Links Mitte',
                'left top' => 'Links oben',
            ]),
            TextField::create('BGPositionOffsetX', 'Horizontaler Versatz')->setDescription('Angaben können in px, rem, em, % oder auto sein.'),
            TextField::create('BGPositionOffsetY', 'Vertikaler Versatz')->setDescription('Angaben können in px, rem, em, % oder auto sein.')
        ]);
    }

    public function BGPositionWithOffsets()
    {
        $xPos = explode(' ', $this->owner->BGPosition)[0];
        $yPos = explode(' ', $this->owner->BGPosition)[1];
        return $xPos . ' ' . ($xPos != 'center' ? $this->owner->BGPositionOffsetX : '') . ' ' . $yPos . ' ' . ($yPos != 'center' ? $this->owner->BGPositionOffsetY : '') . '';
    }

    public function BGSizeNice()
    {
        if ($this->owner->BGSize == 'custom') {
            return $this->owner->BGSizeCustom;
        }
        return $this->owner->BGSize;
    }

    public function CalculatedBGImage($definition = 1, $maxWidth = 0)
    {
        if ($this->owner->BGSize == 'custom') {
            $xSize = explode(" ", $this->owner->BGSizeCustom)[0];
            $ySize = explode(" ", $this->owner->BGSizeCustom)[1];

            if (str_contains($xSize, 'px') || str_contains($ySize, 'px')) {
    
                if(str_contains($xSize, 'px') && str_contains($ySize, 'px')){ 
                    return $this->owner->BGImage()->FocusFill(intval(substr_replace($xSize, "", -2)) * $definition, intval(substr_replace($ySize, "", -2)) * $definition)->URL;
                }else{
                    if (str_contains($xSize, 'px')) {  
                        return $this->owner->BGImage()->ScaleWidth(intval(substr_replace($xSize, "", -2)) * $definition)->URL;           
                    } elseif (str_contains($ySize, 'px')) {  
                        return $this->owner->BGImage()->ScaleHeight(intval(substr_replace($ySize, "", -2)) * $definition)->URL;     
                    }
                }
            }

            return $this->owner->BGImage()->URL;
            
        }elseif ($this->owner->BGSize == 'cover') {
            return $this->owner->BGImage()->ScaleWidth($maxWidth * $definition)->URL;
        }

        return $this->owner->BGImage()->URL;
    }

    public function BGImagesStyles(){
        if($this->owner->BGImage()->exists()){
            return LiteralField::create(null, strip_tags(trim(preg_replace('/\s\s+/', ' ','
            --bg-opacity:'. $this->owner->BGImageOpacity .'; 
            --bg-attachment: '. $this->owner->BGAttachment.';
            --bg-repeat: '. $this->owner->BGRepeat.';
            --bg-position: '. $this->BGPositionWithOffsets().';
            --bg-size: '. $this->BGSizeNice().';
            --bg-mixfilter: '. $this->owner->BGMixFilter.';

            --bg-image: url('. $this->CalculatedBGImage(1, 576).');
            --bg-image-2x: url('. $this->CalculatedBGImage(2, 576).');
            --bg-image-3x: url('. $this->CalculatedBGImage(3, 576).');

            --bg-image-sm: url('. $this->CalculatedBGImage(1, 767).');
            --bg-image-sm-2x: url('. $this->CalculatedBGImage(2, 767).');
            --bg-image-sm-3x: url('. $this->CalculatedBGImage(3, 767).');

            --bg-image-md: url('. $this->CalculatedBGImage(1, 991).');
            --bg-image-md-2x: url('. $this->CalculatedBGImage(2, 991).');
            --bg-image-md-3x: url('. $this->CalculatedBGImage(3, 991).');

            --bg-image-lg: url('. $this->CalculatedBGImage(1, 1199).');
            --bg-image-lg-2x: url('. $this->CalculatedBGImage(2, 1199).');
            --bg-image-lg-3x: url('. $this->CalculatedBGImage(3, 1199).');

            --bg-image-xl: url('. $this->CalculatedBGImage(1, 1399).');
            --bg-image-xl-2x: url('. $this->CalculatedBGImage(2, 1399).');
            --bg-image-xl-3x: url('. $this->CalculatedBGImage(3, 1399).');

            --bg-image-xxl: url('. $this->CalculatedBGImage(1, 1920).');
            --bg-image-xxl-2x: url('. $this->CalculatedBGImage(2, 1920).');
            --bg-image-xxl-3x: url('. $this->CalculatedBGImage(3, 1920).');
        '))));
        }

        return false;
    }
}