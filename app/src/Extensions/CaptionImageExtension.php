<?php

use SilverStripe\ORM\DataExtension;

class CaptionImageExtension extends DataExtension
{
    private static $db = array(
        'Caption' => 'Varchar(255)',
        'SourceAuthor' => 'Varchar(255)',
        'SourceLink' => 'Text',
        'InAttributions' => 'Boolean'
    );

    private static $defaults = [
        'InAttributions' => 0
    ];
}