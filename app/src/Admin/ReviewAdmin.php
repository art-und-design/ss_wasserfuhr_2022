<?php

use SilverStripe\Admin\ModelAdmin;

class ReviewAdmin extends ModelAdmin
{
    private static $managed_models = [
        'Review'
    ];

    private static $menu_icon_class = 'font-icon-comment';

    private static $url_segment = 'reviews';

    private static $menu_title = 'Bewertungen';

    private static $model_importers = [
        'Review' => 'ReviewCSVUploader',
     ];

}