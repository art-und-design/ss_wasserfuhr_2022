<?php

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridField;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ServiceAdmin extends ModelAdmin
{
    private static $managed_models = [
        'Service'
    ];

    private static $menu_icon_class = 'font-icon-checklist';

    private static $url_segment = 'services';

    private static $menu_title = 'Services';

    public function getEditForm($id = null, $fields = null){
        $form = parent::getEditForm($id, $fields);
        $model = singleton($this->modelClass);
        
        if ($model->hasField('SortOrder') && $gridField=$form->Fields()->dataFieldByName($this->sanitiseClassName($this->modelClass))) {
            if($gridField instanceof GridField) {
                $gridField->getConfig()->addComponent(new GridFieldOrderableRows('SortOrder'));
            }
        }

        return $form;
    }
}