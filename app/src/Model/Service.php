<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\View\ArrayData;
use SilverStripe\Forms\TextField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class Service extends DataObject
{

    private static $singular_name = 'Service';
    private static $plural_name = 'Services';

    private static $db = [
        'SortOrder' => 'Int',
        'Title' => 'Varchar(128)',
        'Content' => 'HTMLText',
        'IconClass' => 'Varchar(32)'
    ];

    private static $defaults = [
        'Title' => '',
        'IconClass' => 'info-circle'
    ];

    private static $summary_fields = [
        'Title',
        'Content.FirstParagraph'
    ];

    private static $field_labels = [
        'Title' => 'Titel',
        'Content' => 'Inhalt',
        'TargetLink' => 'Ziel',
        'Content.FirstParagraph' => 'Vorschau'
    ];

    private static $has_one = [
        'TargetLink' => SiteTree::class
    ];

    private static $owns = [];

    private static $default_sort = 'SortOrder ASC';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'SortOrder',
            'Title',
            'IconClass',
            'Content',
            'TargetLinkID'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', $this->fieldLabel('Title')),
            DropdownField::create('IconClass', 'Icon', [
                'comments' => 'Sprechblasen',
                'archive' => 'Karton',
                'baby' => 'Baby',
                'boxes' => 'Kisten',
                'brush' => 'Pinsel',
                'car' => 'Auto',
                'desktop' => 'Monitor',
                'door-open' => 'Offene Tür',
                'fill-drip' => 'Farbeimer',
                'heart' => 'Herz',
                'key' => 'Schlüssel',
                'palette' => 'Palette',
                'mug-hot' => 'Heißes Getränk',
                'percent' => 'Prozent-Symbol',
                'swatchbook' => 'Farbfächer',
                'toilet-paper' => 'Toilettenpapier',
                'users' => 'Personen',
                'truck-pickup' => 'Pickup',
                'thermometer-half' => 'Thermometer',
                'store' => 'Laden',
                'sink' => 'Waschbecken',
                'restroom' => 'WC',
                'info-circle' => 'Info-Symbol',
                'lightbulb' => 'Glühbirne',
                'gifts' => 'Geschenke',
                'couch' => 'Couch',
                'child' => 'Kind',
                'bath' => 'Bad',
                'baby-carriage' => 'Kinderwagen',
                'balance-scale' => 'Waage',
                'camera' => 'Kamera',
                'clock' => 'Uhr',
                'dog' => 'Hund',
                'euro-sign' => 'Euro-Symbol',
                'home' => 'Haus',
                'leaf' => 'Blatt',
                'box-open' => 'Offener Karton',
                'piggy-bank' => 'Sparschwein',
                'trophy' => 'Pokal',
                'trash-alt' => 'Mülltonne',
                'truck' => 'Lieferwagen',
                'truck-loading' => 'Lieferung',
                'wrench' => 'Schraubenschlüssel',
                'toolbox' => 'Werkzeugkiste'
            ])->setHasEmptyDefault(false),
            HTMLEditorField::create('Content', $this->fieldLabel('Content'))->setRows(4),
            TreeDropdownField::create('TargetLinkID', $this->fieldLabel('TargetLink'), SiteTree::class)
        ]);

        return $fields;
    }
}
