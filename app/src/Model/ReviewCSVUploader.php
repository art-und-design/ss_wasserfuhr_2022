<?php

use SilverStripe\Dev\CsvBulkLoader;

class ReviewCSVUploader extends CsvBulkLoader 
{
   public $columnMap = [
      'review_id' => 'ReviewSourceID', 
      'author_title' => 'Author', 
      'review_text' => 'Content', 
      'review_rating' => 'Rating',
      'review_link' => 'ExternalLink', 
      'review_datetime_utc' => '->importRatingDate'
   ];
   
   public $duplicateChecks = [
      'review_id' => 'ReviewSourceID'
   ];
   
   public static function importRatingDate(&$obj, $val, $record) 
   {
      $obj->RatingDate = date("d-m-Y H:i:s", strtotime($val));
   }
}