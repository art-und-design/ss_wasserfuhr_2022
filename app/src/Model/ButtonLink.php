<?php

use gorriecoe\Link\Models\Link;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;

class ButtonLink extends Link
{
    private static $singular_name = 'Button';
    private static $plural_name = 'Buttons';

    private static $db = [
        'Size'  => 'Varchar(8)',
        'Color' => 'Varchar(16)',
        'Outline' => 'Boolean'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Size',
            'Color',
            'Outline'
        ]);

        $fields->addFieldsToTab('Root.Main', [

            DropdownField::create('Size', 'Größe', [
                'btn-sm' => 'Klein',
                'btn-lg' => 'Groß'
            ])->setEmptyString('Normal')->setHasEmptyDefault(true),
            
            DropdownField::create('Color', 'Farbe', [
                '-primary' => 'Primär',
                '-secondary' => 'Sekundär',
                '-dark' => 'Dunkel',
                '-light' => 'Hell',
                '-link' => 'Link'
            ]),
            
            CheckboxField::create('Outline', 'Linien-Stil')
        ]);

        return $fields;
    }
}