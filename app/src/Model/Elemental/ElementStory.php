<?php

use SilverStripe\Assets\Image;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use Bummzack\SortableFile\Forms\SortableUploadField;
use SilverStripe\Forms\CheckboxField;

class ElementStory extends ElementText 
{
    private static $singular_name = 'Story-Element';
    private static $plural_name = 'Story-Elemente';

    private static $icon = 'font-icon-block-story';

    private static $inline_editable = false;

    private static $db = [
        'ExchangeTextImage' => 'Boolean'
    ];

    private static $many_many = [
        'StoryImages' => Image::class
    ];
    
    private static $many_many_extraFields = [
        'StoryImages' => [
            'SortOrder' => 'Int'
        ]
    ];

    private static $owns = [
        'StoryImages'
    ];

    private static $defaults = [];

    public function getType(){
        return "Story";
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'StoryImages',
            'ExchangeTextImage'
        ]);

        $fields->insertAfter('Buttons', SortableUploadField::create('StoryImages', 'Bilder')->setSortColumn('SortOrder'), true);
        $fields->insertAfter('StoryImages', CheckboxField::create('ExchangeTextImage', 'Text & Bild tauschen'), true);

        return $fields;
    }

    public function SortedStoryImages(){
        $itemsNo = $this->StoryImages()->Count();
        $items = $this->StoryImages()->Sort('SortOrder');
        $groupsNo =  ceil($itemsNo / 2);

        $return = new ArrayList();

        for($i = 1; $i <= $groupsNo; $i++){
            $return->add(new ArrayData([
                'GroupIndex' => $i,
                'MultipleOf2' => $i % 2 == 0,
                'StoryImages' => $items->limit(2, ($i-1) * 2)
            ]));
        }


        return $return;
    }
}