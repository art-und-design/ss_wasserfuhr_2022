<?php

use SilverStripe\Forms\TextField;
use gorriecoe\LinkField\LinkField;
use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ElementText extends BaseElement 
{
    private static $singular_name = 'Textelement';
    private static $plural_name = 'Textelemente';

    private static $icon = 'font-icon-block-blog-post';

    private static $inline_editable = false;

    private static $db = [
        'Subtitle' => 'Text',
        'HTML' => 'HTMLText'
    ];

    private static $many_many = [
        'Buttons' => ButtonLink::class
    ];
    
    private static $many_many_extraFields = [
        'Buttons' => [
            'Sort' => 'Int'
        ]
    ];

    private static $defaults = [];

    public function getType(){
        return "Text";
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Subtitle',
            'HTML',
            'Buttons'
        ]);

        $fields->fieldByName('Root.Main.Title')->setTitle('Titel');

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Subtitle', 'Untertitel')->setDescription('Setzen Sie einen Untertitel'),
            HTMLEditorField::create('HTML', 'Inhalt'),
            LinkField::create('Buttons', 'Buttons', $this)
        ]);

        return $fields;
    }
}