<?php

use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataObject;
use SilverStripe\View\ArrayData;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\DatetimeField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\ORM\FieldType\DBField;

class Review extends DataObject
{

    private static $singular_name = 'Bewertung';
    private static $plural_name = 'Bewertungen';

    private static $db = [
        'ReviewSourceID' => 'Text', 
        'Author' => 'Varchar(128)',
        'Content' => 'Text',
        'Rating' => 'Int',
        'ExternalLink' => 'Text',
        'RatingDate' => 'Datetime'
    ];

    private static $defaults = [
        'Author' => '',
        'Rating' => 1
    ];

    private static $summary_fields = [
        'Author',
        'Content.FirstParagraph',
        'RatingNice'
    ];

    private static $field_labels = [
        'Rating' => 'Bewertung',
        'RatingDate' => 'Abgegeben am',
        'Content' => 'Inhalt',
        'Author' => 'Autor',
        'ExternalLink' => 'Externer Link',
        'RatingNice' => 'Bewertung',
        'Content.FirstParagraph' => 'Inhalt'
    ];

    private static $owns = [];

    private static $default_sort = 'RatingDate DESC';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Author',
            'Content',
            'Rating',
            'RatingDate',
            'ReviewSourceID',
            'ExternalLink'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Author', $this->fieldLabel('Title')),
            DatetimeField::create('RatingDate', $this->fieldLabel('RatingDate'))->setHTML5(true),
            TextareaField::create('Content', $this->fieldLabel('Content'))->setRows(4),
            NumericField::create('Rating', $this->fieldLabel('Rating'))->setHTML5(true)->setScale(0),
            TextField::create('ExternalLink', $this->fieldLabel('ExternalLink')),
        ]);

        return $fields;
    }

    public function getRatingStars(){
        $rating = $this->Rating;
        $max = 5;
        $stars = new ArrayList();
        for($i = 0; $i < $max; $i++){
            $stars->add(new ArrayData([
                'Filled' => $i < $rating
            ]));
        }

        return $stars;
    }

    public function getRatingNice(){
        return DBField::create_field(Varchar::class, $this->Rating.' von 5');
    }

    public function getTitle(){
        return DBField::create_field(Varchar::class, $this->Author.' ('.$this->getRatingNice().')');
    }
}
