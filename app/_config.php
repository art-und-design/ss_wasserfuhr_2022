<?php

use SilverStripe\i18n\i18n;
use SilverStripe\Admin\CMSMenu;
use SilverStripe\Security\Member;
use SilverStripe\Reports\ReportAdmin;
use SilverStripe\Security\PasswordValidator;
use SilverStripe\CampaignAdmin\CampaignAdmin;
use SilverStripe\Forms\HTMLEditor\TinyMCEConfig;

// remove PasswordValidator for SilverStripe 5.0
$validator = PasswordValidator::create();
// Settings are registered via Injector configuration - see passwords.yml in framework
Member::set_password_validator($validator);

CMSMenu::remove_menu_class(CampaignAdmin::class);
CMSMenu::remove_menu_class(ReportAdmin::class);

i18n::set_locale('de_DE');

TinyMCEConfig::get('cms')
    ->removeButtons('ssembed', 'sslink', 'anchor', 'code', 'pastetext', 'ssimage', 'ssmedia', 'removeformat', 'underline', 'italic', 'bold', 'bullist', 'numlist', 'formatselect', 'outdent', 'indent', 'table', 'alignleft', 'aligncenter', 'alignright', 'alignjustify', 'paste');

TinyMCEConfig::get('cms')
    ->addButtonsToLine(1, 'formatselect', '|', 'bold', 'italic', 'strikethrough', 'superscript', 'subscript', '|', 'alignleft', 'aligncenter', 'alignright', '|', 'bullist', 'numlist', '|', 'sslink', 'unlink', 'code')
    ->setButtonsForLine(2, '');

TinyMCEConfig::get('cms')
    ->setOptions([
        'block_formats' => 'Header 1=h1;Header 2=h2;Header 3=h3;Header 4=h4;Header 5=h5;Header 6=h6;Paragraph=p;',
        'style_formats_merge' => true,
        'importcss_append' => true,
        'formats' => [
            'aligncenter' => [
                'selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                'classes' => 'text-center'
            ],
            'alignleft' => [
                'selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                'classes' => 'text-start text-left'
            ],
            'alignright' => [
                'selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                'classes' => 'text-end text-right'
            ],
            'bold' => [
                'inline' => 'strong',
                'classes' => 'fw-bold'
            ],
            'italic' => [
                'inline' => 'em'
            ],
            'strikethrough' => [
                'inline' => 'del'
            ],
        ],
    ]);

TinyMCEConfig::get('cms')->setOption(
    'extended_valid_elements',
    'img[class|src|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|usemap],' .
    'iframe[id|onload|allowtransparency|allow|data-src|src|name|width|height|title|align|allowfullscreen|frameborder|marginwidth|marginheight|scrolling],' .
    'object[classid|codebase|width|height|data|type],' .
    'embed[src|type|pluginspage|width|height|autoplay],' .
    'param[name|value],' .
    'map[class|name|id],' .
    'area[shape|coords|href|target|alt],' .
    'ol[start|type],' .
    'del[class|id]'
);
