<header class="bg-primary<% if $HeaderImage %> has-image<% end_if %>">
    <div class="container-lg pt-4 pt-lg-5 <% if not $HeaderImage %> pb-4 pb-lg-5<% end_if %>">

        <h1 class="<% if $HeaderImage %>mb-md-n3 ms-md-5 ms-lg-6<% else %>mb-0<% end_if %> position-relative text-shadow-lg">
            <span class="text-white d-none d-md-inline">$Title</span>    
            <span class="h2 text-white d-md-none">$Title</span>    
        </h1>
        
        <% if $HeaderImage %>
            <picture class="d-block position-relative rounded overflow-hidden">
                <source srcset="$HeaderImage.FocusFill(535,301).URL 1x, $HeaderImage.FocusFill($SiteConfig.Multiply(535,2), $SiteConfig.Multiply(301,2)).URL 2x" media="(max-width: 575px)">
                <source srcset="$HeaderImage.FocusFill(727,312).URL 1x, $HeaderImage.FocusFill($SiteConfig.Multiply(727,2), $SiteConfig.Multiply(312,2)).URL 2x" media="(max-width: 767px)">
                <source srcset="$HeaderImage.FocusFill(951,408).URL 1x, $HeaderImage.FocusFill($SiteConfig.Multiply(951,2), $SiteConfig.Multiply(408,2)).URL 2x" media="(max-width: 991px)">
                <source srcset="$HeaderImage.FocusFill(920,394).URL 1x, $HeaderImage.FocusFill($SiteConfig.Multiply(920,2), $SiteConfig.Multiply(394,2)).URL 2x" media="(max-width: 1199px)">
                <source srcset="$HeaderImage.FocusFill(1100,471).URL 1x, $HeaderImage.FocusFill($SiteConfig.Multiply(1100,2), $SiteConfig.Multiply(471,2)).URL 2x" media="(max-width: 1399px)">
                <source srcset="$HeaderImage.FocusFill(1280,549).URL 1x, $HeaderImage.FocusFill($SiteConfig.Multiply(1280,2), $SiteConfig.Multiply(549,2)).URL 2x" media="(min-width: 1400px)">     
                <img class="img-fluid" width="1280" height="549" src="$HeaderImage.FocusFill(200,100).URL" alt="$HeaderImage.Title" title="$HeaderImage.Title" loading="eager" />                
            </picture>   
        <% end_if %>

    </div>
</header>

