<div class="content-element__content<% if $Style %> $StyleVariant<% end_if %> px-md-6 px-lg-6 mx-lg-6">

	<% if $ShowTitle %>
		<h2 class="content-element__title">
            $Title
            <% if $Subtitle %>
                <span class="content-element__subtitle text-secondary fw-light d-block small">$Subtitle</span>
            <% end_if %>		
	    </h2>
    <% end_if %>
    
	$HTML

    <% if $Buttons %>
        <p class="mb-0">
            <% loop $Buttons.Sort('Sort') %>
                <a href="$LinkURL" title="$Title" $TargetAttr class="btn btn<% if $Outline %>-outline<% end_if %>{$Color} {$Size} mb-1">$Title</a>
            <% end_loop %>
        </p>
    <% end_if %>

</div>