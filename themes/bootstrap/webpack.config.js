const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const webpack = require('webpack');

const devMode = process.env.NODE_ENV !== "production";
console.log('Mode: '+ process.env.NODE_ENV);

module.exports = {
    entry: './src/js/index.js',
    output: {
        path: path.resolve(__dirname, './dist/'),
        filename: devMode ? './bundle.js' : './js/bundle.min.js',
        assetModuleFilename: 'images/[name].[hash][ext][query]',
    },
    devtool: false,
    plugins: [
        new CompressionPlugin(),
        new MiniCssExtractPlugin({
            filename: devMode ? "./css/bundle.css" : "./css/bundle.min.css",
        }),
        new webpack.SourceMapDevToolPlugin({})
        
    ],

    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    devMode ? "style-loader" : MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: { importLoaders: 1 }
                    }, {
                        loader: 'postcss-loader',
                    }, {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.(eot|woff|ttf)$/,
                type: 'asset/resource',
                use: ['file-loader']
            },
            
            {
                test: /\.(jpeg|jpg|png|gif|svg)$/,
                type: 'asset/resource'
            },
            
        ]
    },

    resolve: {
        extensions: ['*', '.js']
    },

    optimization: {
        minimize: !devMode,
    },

    devServer: {
        host: 'localhost',
        watchFiles: ['./templates/**/*.ss'],
        port: 8080,
        //disableHostCheck: true,
        proxy: {
            context: () => true,
            target: 'http://localhost:80',
            secure: true        
        },
        open: true,
        compress: true,
        https: false,
    }
}