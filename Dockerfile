FROM php:7.4-apache

RUN apt-get update && apt-get upgrade -y

## Install ImageMagic
RUN apt-get update \
    && apt-get install -y \
        libmagickwand-dev \
            --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \ 
    && pecl install imagick \ 
    && docker-php-ext-enable \ 
        imagick \
    && apt-get purge -y \
	    libmagickwand-dev 

## Install  xsl zip GMP intl soap Tidy LDAP GD
RUN apt-get update \
    && apt-get install -y \
        libxslt-dev  \
        libzip-dev \
        zip \
        libgmp-dev \
        libicu-dev \
        libxml2-dev \
        libtidy-dev \ 
        libfreetype6-dev \
		libpng-dev \
		libjpeg-dev \
    && docker-php-ext-configure \
        gd \
		    --with-freetype=/usr/include/ \
		    --with-jpeg=/usr/include/ \  
    && docker-php-ext-install -j$(nproc) \
        xsl \
        zip \
        gmp \
        intl \
        soap \
        tidy \
        gd \
        exif \
    && docker-php-ext-enable \
        xsl \
        zip \
        gmp \
        tidy \
        exif \
    && apt-get purge -y \
	    libxslt-dev \
        libzip-dev \
        zip \
        libgmp-dev \
        libicu-dev \
        libtidy-dev \
        libfreetype6-dev \
		libpng-dev \
		libjpeg-dev \
    && rm -rf /var/lib/apt/lists/* 

RUN docker-php-ext-install \
    mysqli \
    bcmath \
    pdo \
    pdo_mysql \
    exif

RUN a2enmod rewrite
RUN service apache2 restart

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer